package com.wipro.microservices.b10.meru.merupromotions.service;

import com.wipro.microservices.b10.meru.merupromotions.dto.PromotionDTO;
import com.wipro.microservices.b10.meru.merupromotions.exception.ResourceNotFoundException;
import com.wipro.microservices.b10.meru.merupromotions.mapper.PromotionMapper;
import com.wipro.microservices.b10.meru.merupromotions.model.Promotion;
import com.wipro.microservices.b10.meru.merupromotions.repository.PromotionRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class PromotionService {
    public static final String PROMOTION_NOT_FOUND_WITH_ID_MESSAGE = "Promotion not found with id %s";

    private final PromotionRepository promotionRepository;

    private final PromotionMapper promotionMapper;

    public PromotionService(PromotionRepository promotionRepository, PromotionMapper promotionMapper) {
        this.promotionRepository = promotionRepository;
        this.promotionMapper = promotionMapper;
    }

    public PromotionDTO create(PromotionDTO promotionDTO) {
        promotionDTO.setId(null);

        Promotion promotion = promotionMapper.toEntity(promotionDTO);

        return promotionMapper.toDto(promotionRepository.save(promotion));
    }

    public PromotionDTO update(Long id, PromotionDTO promotionDTO) {
        promotionDTO.setId(id);

        Optional<Promotion> promotionOptional = promotionRepository.findById(id);
        if(!promotionOptional.isPresent()) {
            throw new ResourceNotFoundException(String.format(PROMOTION_NOT_FOUND_WITH_ID_MESSAGE, id));
        }

        Promotion updatePromotion = promotionMapper.toEntity(promotionDTO);
        Promotion existingPromotion = promotionOptional.get();

        existingPromotion.setName(updatePromotion.getName());
        existingPromotion.setDescription(updatePromotion.getDescription());
        existingPromotion.setDiscountAmount(updatePromotion.getDiscountAmount());
        existingPromotion.setDiscountType(updatePromotion.getDiscountType());
        existingPromotion.setStartDateTime(updatePromotion.getStartDateTime());
        existingPromotion.setEndDateTime(updatePromotion.getEndDateTime());
        existingPromotion.setProducts(updatePromotion.getProducts());

        return promotionMapper.toDto(promotionRepository.save(existingPromotion));
    }

    public void delete(Long id) {
        Optional<Promotion> promotionOptional = promotionRepository.findById(id);
        if (!promotionOptional.isPresent()) {
            throw new ResourceNotFoundException(String.format(PROMOTION_NOT_FOUND_WITH_ID_MESSAGE, id));
        }
        promotionRepository.delete(promotionOptional.get());
    }

    public PromotionDTO findById(Long id) {
        Optional<Promotion> promotionOptional = promotionRepository.findById(id);
        if (!promotionOptional.isPresent()) {
            throw new ResourceNotFoundException(String.format(PROMOTION_NOT_FOUND_WITH_ID_MESSAGE, id));
        }
        return promotionMapper.toDto(promotionOptional.get());
    }

    public List<PromotionDTO> findAll() {
        return promotionRepository.findAll()
                .stream()
                .map(promotionMapper::toDto)
                .collect(Collectors.toList());
    }

    public List<PromotionDTO> findByProduct(String productId) {
        return promotionRepository.findByProduct(productId).stream()
                .map(promotionMapper::toDto)
                .collect(Collectors.toList());
    }
}

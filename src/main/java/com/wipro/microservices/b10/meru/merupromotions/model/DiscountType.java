package com.wipro.microservices.b10.meru.merupromotions.model;

public enum DiscountType {
    VALUE, PERCENTAGE
}

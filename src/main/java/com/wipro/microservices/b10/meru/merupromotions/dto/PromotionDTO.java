package com.wipro.microservices.b10.meru.merupromotions.dto;

import com.wipro.microservices.b10.meru.merupromotions.model.DiscountType;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Set;

@Data
public class PromotionDTO {
    private Long id;

    @NotBlank
    private String name;

    @NotBlank
    private String description;

    @NotEmpty
    private Set<String> products;

    @DecimalMin("0.01")
    private BigDecimal discountAmount;

    @NotNull
    private DiscountType discountType;

    @NotNull
    private LocalDateTime startDateTime;

    @NotNull
    private LocalDateTime endDateTime;
}

package com.wipro.microservices.b10.meru.merupromotions.exception;

import lombok.Data;
import org.springframework.web.bind.MethodArgumentNotValidException;

import javax.validation.ConstraintViolation;
import java.util.List;
import java.util.stream.Collectors;

@Data
public class ErrorResponse {
    private Integer code;
    private String message;
    private String detail;
    private List<String> details;

    public ErrorResponse(Integer code, String message, String detail) {
        this.code = code;
        this.message = message;
        this.detail = detail;
    }

    public ErrorResponse(MethodArgumentNotValidException exception) {
        code = 400;
        message = "Invalid request";
        details = exception.getBindingResult().getFieldErrors()
                .stream()
                .map(e -> e.getField() + ": " + e.getDefaultMessage())
                .collect(Collectors.toList());
    }
}

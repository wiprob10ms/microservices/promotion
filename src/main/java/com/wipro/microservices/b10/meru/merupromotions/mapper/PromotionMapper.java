package com.wipro.microservices.b10.meru.merupromotions.mapper;

import com.wipro.microservices.b10.meru.merupromotions.dto.PromotionDTO;
import com.wipro.microservices.b10.meru.merupromotions.model.Promotion;
import org.springframework.stereotype.Component;

@Component
public class PromotionMapper {

    public PromotionDTO toDto(Promotion entity) {
        PromotionDTO dto = new PromotionDTO();
        dto.setId(entity.getId());
        dto.setName(entity.getName());
        dto.setDescription(entity.getDescription());
        dto.setDiscountAmount(entity.getDiscountAmount());
        dto.setDiscountType(entity.getDiscountType());
        dto.setStartDateTime(entity.getStartDateTime());
        dto.setEndDateTime(entity.getEndDateTime());
        dto.setProducts(entity.getProducts());
        return dto;
    }

    public Promotion toEntity(PromotionDTO dto) {
        Promotion entity = new Promotion();
        entity.setId(dto.getId());
        entity.setName(dto.getName());
        entity.setDescription(dto.getDescription());
        entity.setDiscountAmount(dto.getDiscountAmount());
        entity.setDiscountType(dto.getDiscountType());
        entity.setStartDateTime(dto.getStartDateTime());
        entity.setEndDateTime(dto.getEndDateTime());
        entity.setProducts(dto.getProducts());
        return entity;
    }
}

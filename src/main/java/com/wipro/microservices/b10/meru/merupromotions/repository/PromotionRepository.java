package com.wipro.microservices.b10.meru.merupromotions.repository;

import com.wipro.microservices.b10.meru.merupromotions.model.Promotion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PromotionRepository extends JpaRepository<Promotion, Long> {
    @Query("SELECT a FROM Promotion a JOIN a.products b WHERE b.id = :productId")
    List<Promotion> findByProduct(@Param("productId") String productId);
}

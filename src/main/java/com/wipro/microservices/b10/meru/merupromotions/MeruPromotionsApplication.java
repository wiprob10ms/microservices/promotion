package com.wipro.microservices.b10.meru.merupromotions;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class MeruPromotionsApplication {

	public static void main(String[] args) {
		SpringApplication.run(MeruPromotionsApplication.class, args);
	}

}

package com.wipro.microservices.b10.meru.merupromotions.controller;

import com.wipro.microservices.b10.meru.merupromotions.dto.PromotionDTO;
import com.wipro.microservices.b10.meru.merupromotions.service.PromotionService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
public class PromotionController {

    private final PromotionService promotionService;

    public PromotionController(PromotionService promotionService) {
        this.promotionService = promotionService;
    }

    @PostMapping
    public ResponseEntity<PromotionDTO> create(@RequestBody @Valid PromotionDTO promotionDTO) {
        return ResponseEntity.status(HttpStatus.CREATED).body(promotionService.create(promotionDTO));
    }

    @PutMapping("/{id}")
    public ResponseEntity<PromotionDTO> update(@RequestBody @Valid PromotionDTO promotionDTO, @PathVariable("id") Long id) {
        return ResponseEntity.ok(promotionService.update(id, promotionDTO));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<PromotionDTO> delete(@PathVariable("id") Long id) {
        promotionService.delete(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @GetMapping("/{id}")
    public ResponseEntity<PromotionDTO> findById(@PathVariable("id") Long id) {
        return ResponseEntity.ok(promotionService.findById(id));
    }

    @GetMapping
    public ResponseEntity<List<PromotionDTO>> findAll() {
        return ResponseEntity.ok(promotionService.findAll());
    }

    @GetMapping("/products/{productId}")
    public ResponseEntity<List<PromotionDTO>> findByProductCode(@PathVariable("productId") String productId) {
        return ResponseEntity.ok(promotionService.findByProduct(productId));
    }
}
